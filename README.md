# SWAKIP - Swiss Army knive for imap and pop3

**Code Python3**

```
$ python swakip.py -h
usage: swakip.py [-h] [--type {imap,pop3}] --server SERVER --user USER
                 --passwd PASSWD [--limit LIMIT] [--debug]

Swiss Army knive for imap and pop3

optional arguments:
  -h, --help            show this help message and exit
  --type {imap,pop3}, -t {imap,pop3}
                        precise protocol imap/pop3
  --server SERVER, -s SERVER
                        Server to connect to
  --user USER, -u USER  User to connect with
  --passwd PASSWD, -p PASSWD
                        Password to connect with
  --limit LIMIT, -l LIMIT
                        Limit number of messages to be listed (0 => all)
  --debug, -d           Show all operation results
```

equivalent to [swaks http://www.jetmore.org/john/code/swaks/](http://www.jetmore.org/john/code/swaks/) for imap and pop3 writed in Python (v2 I think x_x' )

this tool aims to test quickly imap and pop3 account from command line, and tries to ease this test by giving telent command lines...


