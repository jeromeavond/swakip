#!/bin/env python3
# coding: utf8

import poplib,imaplib
import argparse
import re
import socket

# Regexp for listing
redate=re.compile("^Date")
resubj=re.compile("^Subject: ")
refrom=re.compile("^From: ")
splitter=re.compile(": ")

# Arguments work
parser = argparse.ArgumentParser(description='Swiss Army knive for imap and pop3')
parser.add_argument('--type','-t',help='precise protocol imap/pop3',choices=['imap', 'pop3'], default='pop3') 
parser.add_argument('--server','-s',help='Server to connect to',required=True) 
parser.add_argument('--SSL','-S',help='Connect via SSL port', action='store_true') 
parser.add_argument('--TLS','-T',help='Connect via STARTTLS', action='store_true') 
parser.add_argument('--user','-u',help='User to connect with',required=True) 
parser.add_argument('--passwd','-p',help='Password to connect with',required=True) 
parser.add_argument('--limit','-l',help='Limit number of messages to be listed (0 => all)', type=int, default=5) 
parser.add_argument('--debug','-d',help='Show all operation results', action='store_true') 

args = parser.parse_args()


# For debugging purpose
# Have to be based on real telnet connection to catch the reals lines
#
def init(server,debug,protocol):
    print("C'est parti %s\n" % protocol)
    if debug:
        print("telnet %s %s" % (server,protocol))
        print("""
Trying %s...
Connected to %s.
Escape character is '^]'.  """ % (socket.gethostbyname(server).decode('utf8'), server))
    if debug and protocol == "pop3":
        print("+OK Dovecot ready.")
    elif debug and protocol == "imap":
        print("* OK [CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE STARTTLS AUTH=PLAIN AUTH=LOGIN AUTH=CRAM-MD5] Dovecot ready.")

# IMAP fonction
# Very simple (too simple ?...) at this hour
# Make a simple connexion with server/user/pass and manage debug and listing
#
def imap(server,user,passwd,limit,debug,SSL):

    # Call initialisation 
    init(server,debug,'imap')

    # Connect to server (like a telnet conn)
    if SSL:
        M=imaplib.IMAP4_SSL(server)
    else:
        M=imaplib.IMAP4(server)

    # With debug set, create a simily telnet dialog
    if debug:
        print( "   > ? LOGIN %s %s" % (user,passwd))
        print( "   %s %s" % M.login(user,passwd))
        print( "   > ? Select " )
        print( "   %s %s" % M.select())
        print( "   > ? LIST \"\" \"*\"")
        print( "   (%s,%s)" % M.search(None, 'ALL'))
    else:
        # After connect : login
        M.login(user,passwd)
        # Then Select first Directory
        M.select()

    # Grep if it's OK or not and the list of messages (table of id)
    typ, data = M.search(None, 'ALL')
    # We manage to get the number of messages
    num = len(data[0].split())
    # We limit the number of mess to be shown
    numMessages = limit if num > limit else num
    # If all ok => set to all the messages
    numMessages = num if limit == 0 else numMessages
    # Cut the id list to the number chosen
    messagesId = data[0].split()[:numMessages]
    # Initialyze date subj and from
    (date,subj,sfrom)=('','','')
    # num take each Id
    for num in messagesId:
        # Fetching mess n°num
        typ, data = M.fetch(num, '(RFC822)')
        if debug:
            print( "   > ? FETCH %s All" % num)
        # Splitting Message by each lines
        #for j in data[0][1].split(u'\r\n'):
        #    # Get only Sentence Start by Date: From: Subject:
        #    if redate.search(j):
        #        date = "%s" % j 
        #    if resubj.search(j):
        #        subj = "%s" %  j 
        #    if refrom.search(j):
        #        sfrom = "%s" %  j 
        ## Print the Listing
        #print( "   %s - %-20s\t%-50s\t- %s " % (num,date,subj,sfrom))
    # Close connection
    M.close()
    M.logout()

def pop3(server,user,passwd,limit,debug):
    init(server,debug,'pop3')
    M=poplib.POP3(server)
    if debug:
        print( "   > user %s" % user)
        print( "   %s" % M.user(user))
        print( "   > pass %s" % passwd)
        print( "   %s" % M.pass_(passwd))
        print( "   > list " )
        print( "   (%s,%s,%s)" % M.list())
    else:
        M.user(user)
        M.pass_(passwd)
        M.list()
    num = len(M.list()[1])
    numMessages = limit if num > limit else num
    numMessages = num if limit == 0 else numMessages
    (date,subj,sfrom)=('','','')
    for i in range(numMessages):
        if debug:
            print( "   > retr %s" % i)
        for j in M.retr(i+1)[1]:
            if redate.search(j):
                date = j 
            if resubj.search(j):
                subj = j 
            if refrom.search(j):
                sfrom = j 
        print( "   %s - %-20s\t%-50s\t- %s " % (i,date,subj,sfrom))


# Call function from type

if args.type == "imap":
    imap(args.server,args.user,args.passwd,args.limit,args.debug,args.SSL)
elif args.type == "pop3":
    pop3(args.server,args.user,args.passwd,args.limit,args.debug)
    
